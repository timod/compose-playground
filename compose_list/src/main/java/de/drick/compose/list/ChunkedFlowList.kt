package de.drick.compose.list

import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.gesture.scrollorientationlocking.Orientation
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*

/**
 * Executes loadPage block until no results are returned and emits the items of the result list to the flow.
 */
/*suspend fun <T> FlowCollector<T>.pageLoader(firstPage: Int = 0, maxPages: Int = 100, loadPage: suspend (Int) -> List<T>) {
    var page = firstPage
    var emittedValues = true
    while (page < maxPages && emittedValues) {
        emittedValues = false
        loadPage(page).fastForEach {
            delay(1000)
            emit(it)
            emittedValues = true
        }
        page++
    }
}*/

class LayoutChunkedFlow<T>(
    val flow: Flow<T>,
    chunks: Int = 2
): CoroutineScope {
    private val job = Job()
    override val coroutineContext = Dispatchers.Main + job

    private val internalList = mutableListOf<T>()
    private var _chunks = chunks

    val listPosition: LayoutListPosition = LayoutListPosition()
    val chunkedList: SnapshotStateList<List<T>> = mutableStateListOf()

    private val channel = Channel<Int>()

    init {
        launch {
            log("$this - Start collecting for $flow")
            flow.buffer(5)
                .collect {
                val lastItem = listPosition.lastVisibleItemState.value
                internalList.add(it)

                synchronized(chunkedList) {
                    val lastIndex = chunkedList.size - 1
                    //Add to list
                    if (lastIndex >= 0 && chunkedList[lastIndex].size < _chunks) { // extend existing chunk
                        chunkedList[lastIndex] = chunkedList[lastIndex] + it
                    } else { // add new chunk
                        chunkedList.add(listOf(it))
                    }
                }

                if (chunkedList.size > lastItem + 2) {
                    log("Wait until new items needed. Collected: ${chunkedList.size} displayed: ${lastItem + 1}")
                    channel.receive()
                    log("item requested")
                }
            }
            log("$this - Collection end reached")
        }
    }

    fun setChunks(count: Int) {
        synchronized(chunkedList) {
            chunkedList.clear()
            chunkedList.addAll(internalList.chunked(count))
            _chunks = count
        }
    }

    protected fun finalize() {
        job.cancel()
        channel.cancel()
        log("$this - Channel and job canceled")
    }

    internal fun endReached(position: Int) {
        launch {
            channel.send(position)
        }
    }
}

@Composable
fun <T : Any> LayoutChunkedList(
    orientation: Orientation = Orientation.Vertical,
    flow: LayoutChunkedFlow<T>, itemCallback: @Composable() (List<T>) -> Unit
) {
    val trigger: (Int) -> Unit = {
        flow.endReached(it)
    }
    LayoutList(orientation, flow.listPosition, trigger, flow.chunkedList, itemCallback)
}
