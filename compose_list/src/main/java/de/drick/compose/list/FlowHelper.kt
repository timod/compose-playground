package de.drick.compose.list

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumnForIndexed
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

/**
 * Executes loadPage until result list is empty and emits the items as flow.
 */
fun <T>pagingFlow(firstPage: Int = 0, maxPages: Int = 100, init: (suspend FlowCollector<T>.() -> Unit)? = null, loadPage: suspend (Int) -> List<T>): Flow<T> {
    var page = firstPage
    return flow {
        init?.invoke(this)
        var emittedValues = true
        while (page < maxPages && emittedValues) {
            emittedValues = false
            loadPage(page).forEach {
                emit(it)
                emittedValues = true
            }
            page++
        }
    }
}


fun <T> Flow<T>.chunked(size: Int) = flow<List<T>> {
    var buffer = ArrayList<T>(size)
    collect { item ->
        buffer.add(item)
        if (buffer.size >= size) {
            emit(buffer)
            buffer = ArrayList(size)
        }
    }
    if (buffer.size > 0) {
        emit(buffer)
    }
}

@Composable
fun <T> FlowColumnFor(
    flow2StateList: Flow2StateList<T>,
    modifier: Modifier = Modifier,
    state: LazyListState = rememberLazyListState(),
    contentPadding: PaddingValues = PaddingValues(0.dp),
    horizontalAlignment: Alignment.Horizontal = Alignment.Start,
    itemContent: @Composable LazyItemScope.(FlowState<T>) -> Unit
) {
    LazyColumnForIndexed(
        items = flow2StateList.list,
        modifier = modifier,
        state = state,
        contentPadding = contentPadding,
        horizontalAlignment = horizontalAlignment
    ) { index, item ->
        flow2StateList.requestListSize(index + 1)
        itemContent(this, item)
    }
}

@Composable
fun <T> FlowMultiColumnFor(columns: Int, stateList: Flow2StateList<T>, modifier: Modifier = Modifier, girdPadding: Dp = 0.dp, state: LazyListState, itemCallback: @Composable (FlowState<T>) -> Unit) {
    val buffer = 10
    LazyColumnForIndexed(
        items = stateList.list.chunked(columns),
        modifier = modifier,
        state = state,
        contentPadding = PaddingValues(girdPadding)
    ) { index, rowList ->
        stateList.requestListSize((index + 1 + buffer) * columns)
        Row(Modifier.padding(top = girdPadding, start = girdPadding)) {
            val rowModifier = Modifier.weight(1f).padding(end = girdPadding).align(Alignment.CenterVertically)
            rowList.forEach {
                Box(rowModifier) {
                    itemCallback(it)
                }
            }
            val emptyRows = (columns - rowList.size)
            repeat(emptyRows) {
                Spacer(modifier = rowModifier)
            }
        }
    }
}

fun <T> Flow<T>.stateList() = Flow2StateList(this)

sealed class FlowState<out T> {
    object Loading : FlowState<Nothing>()
    class Error(val error: Throwable): FlowState<Nothing>()
    class Item<T>(val value: T): FlowState<T>()
}

class Flow2StateList<T>(
    private val flow: Flow<T>
): CoroutineScope {
    private val job = Job()
    override val coroutineContext = Dispatchers.Main + job
    val list: SnapshotStateList<FlowState<T>> = mutableStateListOf()

    private val requestItemsChannel = Channel<Int>(Channel.CONFLATED)
    private val retryChannel = Channel<Boolean>(Channel.CONFLATED)
    private var requestedListSize = 1

    init {
        launch {
            log("$this - Start collecting for $flow")
            if (requestedListSize <= 0) {
                requestedListSize = requestItemsChannel.receive()
            }
            list.add(FlowState.Loading) // set last list item to loading
            var inProgress = true

            flow.buffer(5)
                .retryWhen { cause, _ ->
                    log("Retry", cause)
                    if (inProgress) {
                        list.removeLast()
                    }
                    list.add(FlowState.Error(cause)) // Add error item to end of list
                    inProgress = true
                    return@retryWhen retryChannel.receive()
                }.collect {
                    if (inProgress) {
                        list.removeLast()
                        inProgress = false
                    }
                    list.add(FlowState.Item(it))
                    if (list.size >= requestedListSize) {
                        log("Wait until new items needed. List size: ${list.size} requested: $requestedListSize")
                        requestedListSize = requestItemsChannel.receive()
                    }
                }
            if (inProgress) {
                list.removeLast()
                inProgress = false
            }
            log("$this - collection ready")
        }
    }

    /**
     * Request more items to consume from the flow
     */
    fun requestItems(count: Int) {
        if (job.isActive) {
            launch {
                val requestSize = list.size + count
                log("Request items. List size: ${list.size} requested: $requestSize")
                requestItemsChannel.send(requestSize)
            }
        }
    }
    fun requestListSize(requestSize: Int) {
        if (job.isActive) {
            launch {
                log("Request items. List size: ${list.size} requested: $requestSize")
                if (list.size < requestSize) {
                    log("Send request")
                    requestItemsChannel.send(requestSize)
                }
            }
        }
    }

    fun retry() {
        launch {
            retryChannel.send(true)
        }
    }

    protected fun finalize() {
        job.cancel()
        requestItemsChannel.cancel()
        retryChannel.cancel()
        log("$this - Channel and job canceled")
    }

}
