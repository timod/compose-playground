package de.drick.compose.list

import androidx.compose.animation.animatedFloat
import androidx.compose.animation.animatedValue
import androidx.compose.animation.core.*
import androidx.compose.foundation.animation.FlingConfig
import androidx.compose.foundation.animation.fling
import androidx.compose.foundation.gestures.draggable
import androidx.compose.runtime.*
import androidx.compose.ui.*
import androidx.compose.ui.gesture.scrollorientationlocking.Orientation
import androidx.compose.ui.layout.LayoutModifier
import androidx.compose.ui.layout.Measurable
import androidx.compose.ui.layout.MeasureResult
import androidx.compose.ui.layout.MeasureScope
import androidx.compose.ui.unit.Constraints
import kotlin.math.min
import kotlin.math.roundToInt

@Composable
fun Modifier.swipeToRemove(
    orientation: Orientation = Orientation.Horizontal,
    removedState: MutableState<Boolean> = remember { mutableStateOf(false) },
    onRemoved: () -> Unit,
    enabled: Boolean = true
) = composed {
    val positionOffset = animatedFloat(0f)
    val animatedCollapseSize = animatedValue(initVal = 0, converter = Int.VectorConverter)
    var removed by removedState
    var swipeSizeF by remember { mutableStateOf(0f) }
    var collapseSize by remember { mutableStateOf(0) }

    Modifier.draggable(
        startDragImmediately = positionOffset.isRunning,
        orientation = orientation,
        onDragStopped = { velocity ->
            val config = FlingConfig(anchors = listOf(-swipeSizeF, 0f, swipeSizeF))
            positionOffset.fling(startVelocity = velocity, config = config) { _, endValue, _ ->
                //log("Fling end $endValue")
                if (endValue != 0f) {
                    animatedCollapseSize.snapTo(collapseSize)
                    animatedCollapseSize.animateTo(0, onEnd = { _, _ ->
                        //log("Collapsed")
                        removed = true
                        onRemoved()
                        positionOffset.snapTo(0f)
                    }, anim = tween(500))
                }
            }
        },
        onDrag = { delta ->
            positionOffset.snapTo(positionOffset.value + delta)
        },
        enabled = enabled
    ).then(object : LayoutModifier {
        override fun MeasureScope.measure(
            measurable: Measurable, constraints: Constraints
        ): MeasureResult {
            val childPlaceable = measurable.measure(constraints)
            val swipeSize = when (orientation) {
                Orientation.Horizontal -> {
                    collapseSize = childPlaceable.height
                    constraints.maxWidth
                }
                Orientation.Vertical -> {
                    collapseSize = childPlaceable.width
                    constraints.maxHeight
                }
            }
            swipeSizeF = swipeSize.toFloat()
            positionOffset.setBounds(-swipeSizeF, swipeSizeF)
            val lwidth = when (orientation) {
                Orientation.Horizontal -> childPlaceable.width
                Orientation.Vertical -> if (animatedCollapseSize.isRunning || removed) animatedCollapseSize.value else childPlaceable.width
            }
            val lheight = when (orientation) {
                Orientation.Vertical -> childPlaceable.height
                Orientation.Horizontal -> if (animatedCollapseSize.isRunning || removed) animatedCollapseSize.value else childPlaceable.height
            }
            return layout(lwidth, lheight) {
                if (removed.not()) {
                    if (orientation == Orientation.Vertical)
                        childPlaceable.place(0, positionOffset.value.roundToInt())
                    else
                        childPlaceable.place(positionOffset.value.roundToInt(), 0)
                }
            }
        }
    }
    )
}


enum class CollapseState {
    COLLAPSED, EXPANDED
}

@Composable
fun Modifier.expandable(
    id: Any?,
    collapsed: CollapseState,
    onChanged: (CollapseState) -> Unit = {},
    duration: Int = 500
) = collapseable(id, collapsed, onChanged, duration, CollapseState.COLLAPSED)

@Composable
fun Modifier.collapseable(
    id: Any?,
    collapsed: CollapseState,
    onChanged: (CollapseState) -> Unit = {},
    duration: Int = 500,
    initialState: CollapseState = CollapseState.EXPANDED
) = composed {
    val animatedHeight = animatedValue(initVal = 0, converter = Int.VectorConverter)
    var isRunning by stateFor(id) { false }
    val originalHeightState = stateFor(id) { 0 }
    var currentState by stateFor(id) { initialState }
    val heightState = stateFor(id) { 0 }

    if (isRunning.not()) animatedHeight.snapTo(if (currentState == CollapseState.EXPANDED) heightState.value else 0)
    if (collapsed != currentState) {
        //State change requested
        val animation = tween<Int>(duration)
        isRunning = true
        if (collapsed == CollapseState.COLLAPSED) {
            // Collapse requested
            animatedHeight.animateTo(
                0,
                onEnd = { _, _ -> onChanged(CollapseState.COLLAPSED); isRunning = false })
        } else {
            animatedHeight.animateTo(
                originalHeightState.value,
                animation,
                onEnd = { _, _ -> onChanged(CollapseState.EXPANDED); isRunning = false })
        }
        currentState = collapsed
    }
    CollapseableModifier(originalHeightState, animatedHeight, heightState)
}

private data class CollapseableModifier(
    val originalHeightState: MutableState<Int>,
    val animatedHeight: AnimatedValue<Int, AnimationVector1D>,
    val height: MutableState<Int>
) : LayoutModifier {
    override fun MeasureScope.measure(
        measurable: Measurable,
        constraints: Constraints
    ): MeasureResult {
        val childConstraints =
            constraints.copy(maxHeight = Int.MAX_VALUE, maxWidth = constraints.maxWidth)
        val placeable = measurable.measure(childConstraints)
        originalHeightState.value = placeable.height
        val width = min(placeable.width, constraints.maxWidth)
        height.value = min(placeable.height, constraints.maxHeight)

        return layout(width, animatedHeight.value) {
            placeable.place(0, 0)
        }
    }
}
