package de.drick.compose.list

import androidx.compose.animation.asDisposableClock
import androidx.compose.animation.core.AnimationClockObservable
import androidx.compose.animation.core.AnimationEndReason
import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.SpringSpec
import androidx.compose.foundation.*
import androidx.compose.foundation.animation.FlingConfig
import androidx.compose.foundation.animation.defaultFlingConfig
import androidx.compose.foundation.gestures.ScrollableController
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.*
import androidx.compose.runtime.savedinstancestate.Saver
import androidx.compose.runtime.savedinstancestate.mapSaver
import androidx.compose.runtime.savedinstancestate.rememberSavedInstanceState
import androidx.compose.ui.Modifier
import androidx.compose.ui.gesture.scrollorientationlocking.Orientation
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.Placeable
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.platform.AnimationClockAmbient
import kotlin.math.max
import kotlin.math.min

@Composable
fun <T>state(init: () -> T) = remember { mutableStateOf(init()) }

@Composable
fun <T, V1>stateFor(key: V1, init: () -> T) = remember(key) { mutableStateOf(init()) }


class LayoutListPosition(
    var firstVisibleItemState: MutableState<Int> = mutableStateOf(0),
    var lastVisibleItemState: MutableState<Int> = mutableStateOf(0),
    var scrollOffset: MutableState<Float> = mutableStateOf(0f)
) {
    companion object {
        /**
         * The default [Saver] implementation for [ScrollState].
         */
        fun Saver(): Saver<LayoutListPosition, *> = mapSaver(
            save = { mapOf(
                "first" to it.firstVisibleItemState.value,
                "last" to it.lastVisibleItemState.value,
                "offset" to it.scrollOffset.value
            ) },
            restore = { LayoutListPosition(
                firstVisibleItemState = mutableStateOf(it["first"] as Int),
                lastVisibleItemState = mutableStateOf(it["last"] as Int),
                scrollOffset = mutableStateOf(it["offset"] as Float)
            ) }
        )
    }
}

@Composable
fun layoutListPosition() = rememberSavedInstanceState(saver = LayoutListPosition.Saver()) { LayoutListPosition() }

@Composable
fun <T : Any> LayoutFlowList(
    listPosition: LayoutListPosition = layoutListPosition(),
    orientation: Orientation = Orientation.Vertical,
    flow: Flow2StateList<T>, itemCallback: @Composable() (FlowState<T>) -> Unit
) {
    val trigger: (Int) -> Unit = remember(flow) {{
        flow.requestItems(10)
    }}
    if (flow.list.size < 5) {
        flow.requestItems(5)
    }
    LayoutList(orientation, listPosition, trigger, flow.list, itemCallback)
}

@Composable
fun <T : Any> VerticalLayoutList(
    listPosition: LayoutListPosition = layoutListPosition(),
    items: List<T>, itemCallback: @Composable() (T) -> Unit
) = LayoutList(Orientation.Vertical, listPosition, null, items, itemCallback)

@Composable
fun <T : Any> HorizontalLayoutList(
    listPosition: LayoutListPosition = layoutListPosition(),
    items: List<T>, itemCallback: @Composable() (T) -> Unit
) = LayoutList(Orientation.Horizontal, listPosition, null, items, itemCallback)

@Composable
fun <T: Any>LayoutList(orientation: Orientation = Orientation.Vertical,
                       listPosition: LayoutListPosition = layoutListPosition(),
                       onEndReached:  ((Int) -> Unit)? = null,
                       items: List<T>, itemCallback: @Composable() (T) -> Unit) {
    var firstVisibleItem by listPosition.firstVisibleItemState
    var lastVisibleItem by listPosition.lastVisibleItemState
    var scrollOffset by listPosition.scrollOffset
    var scrollOffsetItem by state { listPosition.firstVisibleItemState.value }
    var removedNodeSize by state { 0f }

    val scrollerPosition = rememberScrollStateInternal(scrollOffset)

    if (items.isNotEmpty()) {
        Layout(
            content = {
                firstVisibleItem = min(firstVisibleItem, items.size - 1)
                lastVisibleItem = min(lastVisibleItem, items.size - 1)
                for (i in firstVisibleItem..lastVisibleItem) {
                    key(i) {
                        Box(Modifier.layoutId(i)) {
                            itemCallback(items[i])
                        }
                    }
                }
                if (lastVisibleItem >= items.size - 1) {
                    onEndReached?.invoke(lastVisibleItem)
                }
            },
            modifier = Modifier.scrollable(
                orientation = orientation,
                controller = scrollerPosition.scrollableController,
                reverseDirection = true
            )
        ) { measurables, constraints ->
            val childConstraints =
                if (orientation == Orientation.Vertical)
                    constraints.copy(maxHeight = Int.MAX_VALUE, maxWidth = constraints.maxWidth)
                else
                    constraints.copy(maxHeight = constraints.maxHeight, maxWidth = Int.MAX_VALUE)
            val parentSize =
                if (orientation == Orientation.Vertical) constraints.maxHeight else constraints.maxWidth
            val threshold = 200
            var sumSize = 0
            val placeables = measurables.map {
                it.measure(childConstraints).apply {
                    sumSize += size(orientation)
                }
            }
            val currentScrollIndex = measurables.first().layoutId as Int
            val newMax = max(0f, (sumSize - parentSize).toFloat())
            val diff = scrollOffsetItem - currentScrollIndex
            if (diff > 0) { // item added
                scrollerPosition.maxValue =
                    newMax // before scrolling we must update the max position.
                scrollerPosition.scrollBy(placeables.first().sizeF(orientation))
                scrollOffsetItem = firstVisibleItem
            }
            if (diff < 0) {
                scrollerPosition.scrollBy(-removedNodeSize)
                scrollOffsetItem = firstVisibleItem
            }
            val tmp = scrollerPosition.value
            if (tmp != scrollOffset) scrollOffset = tmp
            val scroll = tmp.toInt()
            // only check for removing/adding items when first and last visible items are equal to the measurables
            // because than the measurables are in sync with the children defined above
            if (firstVisibleItem == measurables.first().layoutId && lastVisibleItem == measurables.last().layoutId) {
                val firstSize = placeables.first().size(orientation)
                val lastSize = placeables.last().size(orientation)
                scrollerPosition.maxValue = newMax
                if (scroll <= threshold && firstVisibleItem > 0) { // add item in front of the list
                    firstVisibleItem--
                    // unfortunately we do not know the height of the inserted item.
                    // so we must detect the new item in the next measure run
                    //log("FIRST-- scroll: $scroll")
                }
                // check if we can increase the first visible item
                if (scroll > firstSize + lastSize + threshold) { // remove invisible item
                    firstVisibleItem++
                    removedNodeSize = firstSize.toFloat()
                    //log("FIRST++ scroll: $scroll firstHeight: $removedNodeHeight")
                }
                // check if we must increase last visible item
                val pixelsInvisible = sumSize - parentSize - scroll // invisible pixels left
                if (pixelsInvisible <= threshold && lastVisibleItem < items.size - 1) {
                    lastVisibleItem++
                    //log("LAST++")
                }
                // check if we can decrease the last visible item
                if (pixelsInvisible > lastSize + firstSize + threshold && lastVisibleItem > 0) {
                    lastVisibleItem--
                    //log("LAST--")
                }
            }
            layout(constraints.maxWidth, constraints.maxHeight) {
                var currentPos = -scroll
                placeables.forEach { placeable ->
                    if (orientation == Orientation.Vertical) {
                        placeable.place(0, currentPos)
                        currentPos += placeable.height
                    } else {
                        placeable.place(currentPos, 0)
                        currentPos += placeable.width
                    }
                }
            }
        }
    }
}

fun Placeable.sizeF(orientation: Orientation) = if (orientation == Orientation.Vertical) height.toFloat() else width.toFloat()
fun Placeable.size(orientation: Orientation) = if (orientation == Orientation.Vertical) height else width

@Composable
fun rememberScrollStateInternal(initial: Float = 0f): ScrollStateInternal {
    val clock = AnimationClockAmbient.current.asDisposableClock()
    val config = defaultFlingConfig()
    return rememberSavedInstanceState(
        clock, config,
        saver = ScrollStateInternal.Saver(config, clock)
    ) {
        ScrollStateInternal(
            flingConfig = config,
            initial = initial,
            animationClock = clock
        )
    }
}

/**
 * State of the scroll. Allows the developer to change the scroll position or get current state by
 * calling methods on this object. To be hosted and passed to [ScrollableRow], [ScrollableColumn],
 * [Modifier.verticalScroll] or [Modifier.horizontalScroll]
 *
 * To create and automatically remember [ScrollState] with default parameters use
 * [rememberScrollState].
 *
 * Learn how to control [ScrollableColumn] or [ScrollableRow]:
 * @sample androidx.ui.foundation.samples.ControlledScrollableRowSample
 *
 * @param initial value of the scroll
 * @param flingConfig fling configuration to use for flinging
 * @param animationClock animation clock to run flinging and smooth scrolling on
 */
@Stable
class ScrollStateInternal(
    initial: Float,
    internal val flingConfig: FlingConfig,
    animationClock: AnimationClockObservable
) {

    /**
     * current scroll position value in pixels
     */
    var value by mutableStateOf(initial, structuralEqualityPolicy())
        private set

    /**
     * maximum bound for [value], or [Float.POSITIVE_INFINITY] if still unknown
     */
    var maxValue: Float
        get() = _maxValueState.value
        internal set(newMax) {
            _maxValueState.value = newMax
            if (value > newMax) {
                value = newMax
            }
        }

    private var _maxValueState = mutableStateOf(Float.POSITIVE_INFINITY, structuralEqualityPolicy())

    internal val scrollableController =
        ScrollableController(
            flingConfig = flingConfig,
            animationClock = animationClock,
            consumeScrollDelta = {
                val absolute = (value + it)
                val newValue = absolute.coerceIn(0f, maxValue)
                val consumed = newValue - value
                value += consumed
                consumed
            })

    /**
     * Stop any ongoing animation, smooth scrolling or fling occurring on this [ScrollState]
     */
    fun stopAnimation() {
        scrollableController.stopAnimation()
    }

    /**
     * whether this [ScrollState] is currently animating/flinging
     */
    val isAnimationRunning
        get() = scrollableController.isAnimationRunning

    /**
     * Smooth scroll to position in pixels
     *
     * @param value target value in pixels to smooth scroll to, value will be coerced to
     * 0..maxPosition
     * @param spec animation curve for smooth scroll animation
     * @param onEnd callback to be invoked when smooth scroll has finished
     */
    fun smoothScrollTo(
        value: Float,
        spec: AnimationSpec<Float> = SpringSpec(),
        onEnd: (endReason: AnimationEndReason, finishValue: Float) -> Unit = { _, _ -> }
    ) {
        smoothScrollBy(value - this.value, spec, onEnd)
    }

    /**
     * Smooth scroll by some amount of pixels
     *
     * @param value delta in pixels to scroll by, total value will be coerced to 0..maxPosition
     * @param spec animation curve for smooth scroll animation
     * @param onEnd callback to be invoked when smooth scroll has finished
     */
    fun smoothScrollBy(
        value: Float,
        spec: AnimationSpec<Float> = SpringSpec(),
        onEnd: (endReason: AnimationEndReason, finishValue: Float) -> Unit = { _, _ -> }
    ) {
        scrollableController.smoothScrollBy(value, spec, onEnd)
    }

    /**
     * Instantly jump to position in pixels
     *
     * @param value target value in pixels to jump to, value will be coerced to 0..maxPosition
     */
    fun scrollTo(value: Float) {
        this.value = value.coerceIn(0f, maxValue)
    }

    /**
     * Instantly jump by some amount of pixels
     *
     * @param value delta in pixels to jump by, total value will be coerced to 0..maxPosition
     */
    fun scrollBy(value: Float) {
        scrollTo(this.value + value)
    }

    companion object {
        /**
         * The default [Saver] implementation for [ScrollState].
         */
        fun Saver(
            flingConfig: FlingConfig,
            animationClock: AnimationClockObservable
        ): Saver<ScrollStateInternal, *> = Saver<ScrollStateInternal, Float>(
            save = { it.value },
            restore = { ScrollStateInternal(it, flingConfig, animationClock) }
        )
    }
}
