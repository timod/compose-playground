package de.appsonair

import android.os.Handler
import android.os.Looper
import androidx.compose.runtime.*
import de.drick.compose.list.stateFor
import kotlin.properties.ReadOnlyProperty
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * @author Timo Drick
 */
typealias OT<T> = (T) -> Unit

// convenience functions
fun <T>observable(value: T) = ObservableMutableData(value)
fun <T>ObservableMutableData<T>.toObservableData(): ObservableData<T> = this

/**
 * This observe function only trigger a recompose when a new instance replaced the current one.
 */
@Composable
fun <T> observe(data: ObservableData<T>): T {
    var result by stateFor(data) { data.value }

    onCommit(data) {
        val observer = { value: T -> result = value }
        data.subscribe(observer)
        onDispose { data.unsubscribe(observer) }
    }

    return result
}

@Composable
fun <T> observeList(data: ObservableData<T>, itemCallback: @Composable() (T) -> Unit) {
    val invalidationPoint = invalidate
    var value by stateFor(data) { data.value }
    onCommit(data) {
        val observer = { v: T ->
            value = v
            invalidationPoint.invoke()
        }
        data.subscribe(observer)
        onDispose { data.unsubscribe(observer) }
    }
    itemCallback(value)
}


interface ObservableData<T>: ReadOnlyProperty<Any, T> {
    val value: T
    override fun getValue(thisRef: Any, property: KProperty<*>): T
    fun subscribe(observer: OT<T>)
    fun unsubscribe(observer: OT<T>)
}

class ObservableMutableData<T>(initial: T): ObservableData<T>, ReadWriteProperty<Any, T> {
    private var v: T = initial
    override var value
        get() = v
        set(newValue) { v = newValue; notifyObservers(newValue) }

    override fun getValue(thisRef: Any, property: KProperty<*>) = v
    override fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
        this.value = value
    }

    private val handler: Handler = Handler(Looper.getMainLooper())
    private val observerList = mutableSetOf<OT<T>>()

    override fun subscribe(observer: OT<T>) {
        observerList.add(observer)
    }

    override fun unsubscribe(observer: OT<T>) {
        observerList.remove(observer)
    }

    fun notifyObservers() {
        notifyObservers(value)
    }

    private fun notifyObservers(value: T) {
        handler.post {
            observerList.forEach { it(value) }
        }
    }
}
