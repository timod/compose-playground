package de.appsonair

/**
 * apps on air
 *
 * @author Timo Drick
 */
import android.util.Log
import com.example.composeplayground.BuildConfig

const val logFileName = "log.kt"

fun log(error: Throwable) {
    log(error.message ?: "Error:", error)
}

fun log(msg: Any, error: Throwable? = null) {
    if (BuildConfig.DEBUG.not()) return
    val ct = Thread.currentThread()
    val tname = ct.name
    val traces = ct.stackTrace
    val max = traces.size-1
    val stackTrace = traces.slice(3..max).find { it.fileName != logFileName }
    val message = if (stackTrace != null) {
        val cname = stackTrace.className.substringAfterLast(".")
        "[${stackTrace.fileName}:${stackTrace.lineNumber}] $cname.${stackTrace.methodName} : $msg"
    } else {
        "$msg"
    }
    if (error == null) {
        Log.d(tname, message)
    } else {
        Log.d(tname, message, error)
    }
}
