package de.appsonair

import android.os.Handler
import android.os.Looper

// convenience functions
//fun <T>observable(value: T) = ObservableMutableData(value)
//fun <T>ObservableMutableData<T>.toObservableData(): ObservableData<T> = this

/*@Composable
fun <T> observe(data: ObservableData<T>): T {
    var result by stateFor(data) { data.value }

    onCommit(data) {
        val observer = { value: T -> result = value }
        data.subscribe(observer)
        onDispose { data.unsubscribe(observer) }
    }

    return result
}*/

sealed class TaskState<T> {
    class Init<T>: TaskState<T>()
    class Start<T>: TaskState<T>()
    class InProgress<T>: TaskState<T>()
    class Error<T>(val error: Exception): TaskState<T>()
    class Ready<T>(val data: T): TaskState<T>()
}

interface Task<T> {
    val value: TaskState<T>
    fun start()
    fun subscribe(observer: TASKOBSERVER<T>)
    fun unsubscribe(observer: TASKOBSERVER<T>)
}

typealias TASKOBSERVER<T> = (TaskState<T>) -> Unit

class MutableTask<T>(task: () -> T): Task<T> {
    private var v: TaskState<T> = TaskState.Init()
    override var value
        get() = v
        set(newValue) { v = newValue; notifyObservers(newValue) }

    private val handler: Handler = Handler(Looper.getMainLooper())
    private val observerList = mutableSetOf<TASKOBSERVER<T>>()

    override fun start() {

    }

    override fun subscribe(observer: TASKOBSERVER<T>) {
        observerList.add(observer)
    }

    override fun unsubscribe(observer: TASKOBSERVER<T>) {
        observerList.remove(observer)
    }

    private fun notifyObservers(value: TaskState<T>) {
        handler.post {
            observerList.forEach { it(value) }
        }
    }
}
