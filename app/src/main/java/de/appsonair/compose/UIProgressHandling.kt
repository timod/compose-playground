package de.appsonair.compose

import androidx.compose.runtime.*
import kotlinx.coroutines.*

sealed class ProgressState<out T: Any> {
    object Working: ProgressState<Nothing>()
    class Error(val error: Throwable): ProgressState<Nothing>()
    class Result<T: Any>(val value: T): ProgressState<T>()
}

@Composable
fun <T: Any> progressStateFor(vararg inputs: Any?, initBlock: () -> ProgressState<T> = { ProgressState.Working }, onDispose: (ProgressState<T>) -> Unit = {},
                              progressBlock: suspend CoroutineScope.() -> T): ProgressState<T> {
    var state by remember(inputs = inputs) { mutableStateOf(initBlock()) }
    if (state !is ProgressState.Result) {
        LaunchedTask(keys = inputs) {
            state = try {
                ProgressState.Result(progressBlock())
            } catch (err: Throwable) {
                ProgressState.Error(err)
            }
        }
    }
    onDispose {
        onDispose(state)
    }
    return state
}

data class Progress(var state: State) {
    sealed class State {
        object Working : State()
        class Error(val error: Throwable): State()
        object Finished: State()
    }
}

interface ProgressControl {
    val progress: Progress
    fun cancel()
    fun retry()
}

fun progress(progress: Progress = Progress(Progress.State.Working),
             scope: CoroutineScope = CoroutineScope(Dispatchers.Main.immediate),
             block: suspend () -> Unit): ProgressControl {
    val operation: () -> Job = {
        scope.launch {
            progress.state = Progress.State.Working
            progress.state = try {
                block()
                Progress.State.Finished
            } catch (err: java.lang.Exception) {
                Progress.State.Error(err)
            }
        }
    }
    return object : ProgressControl {
        private var job: Job = operation()
        override val progress = progress
        override fun cancel() {
            job.cancel()
        }
        override fun retry() {
            job = operation()
        }
    }
}
