package de.appsonair.compose

import androidx.compose.animation.animatedFloat
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.*
import androidx.compose.runtime.savedinstancestate.UiSavedStateRegistry
import androidx.compose.runtime.savedinstancestate.UiSavedStateRegistryAmbient
import androidx.compose.ui.*
import androidx.compose.ui.draw.drawOpacity
import androidx.compose.ui.layout.LayoutModifier
import androidx.compose.ui.layout.Measurable
import androidx.compose.ui.layout.MeasureResult
import androidx.compose.ui.layout.MeasureScope
import androidx.compose.ui.unit.Constraints
import de.appsonair.log
import kotlin.math.min
import kotlin.math.roundToInt
import kotlin.reflect.KClass

data class SavedStateData<T>(val data: T, var savedState: Map<String, List<Any?>>?)

data class Transition<T>(val data: SavedStateData<T>, val effect: CrossTransitionModifier?)

data class RelativeOffsetPxModifier(val x: Float = 0f, val y: Float = 0f) : LayoutModifier {
    override fun MeasureScope.measure(
        measurable: Measurable,
        constraints: Constraints
    ): MeasureResult {
        val placeable = measurable.measure(constraints)
        return layout(placeable.width, placeable.height) {
            placeable.place((x * placeable.width).roundToInt(), (y * placeable.height).roundToInt())
        }
    }
}

val tweenAnimation = tween<Float>(400)

enum class ItemTransitionState {
    BecomingVisible, BecomingNotVisible;
}

val slideRollRightTransition: CrossTransitionModifier = { progress, state ->
    val p = when(state) {
        ItemTransitionState.BecomingVisible -> -(1f - progress)
        else -> progress
    }
    RelativeOffsetPxModifier(x = p)
}
val slideRollLeftTransition: CrossTransitionModifier = { progress, state ->
    val p = when(state) {
        ItemTransitionState.BecomingVisible -> 1f - progress
        else -> -progress
    }
    RelativeOffsetPxModifier(x = p)
}
val slideInTransition: CrossTransitionModifier = { progress, state ->
    val target = state == ItemTransitionState.BecomingVisible
    val p = if (target) 1f-progress else 0f
    val zIndex = if (target) 1f else 0f
    RelativeOffsetPxModifier(y = p).zIndex(zIndex)
}
val slideOutTransition: CrossTransitionModifier = { progress, state ->
    val target = state == ItemTransitionState.BecomingVisible
    val p = if (target) 0f else progress
    val zIndex = if (target) 0f else 1f
    RelativeOffsetPxModifier(y = p).zIndex(zIndex)
}

val crossfadeTransition: CrossTransitionModifier = { progress, state ->
    val p = if (state == ItemTransitionState.BecomingVisible) progress else 1f - progress
    Modifier.drawOpacity(p)
}
val fadeInTransition: CrossTransitionModifier = { progress, state ->
    val target = state == ItemTransitionState.BecomingVisible
    val p = if (target) min(progress, .99f) else 1f
    val zIndex = if (target) 1f else 0f
    Modifier.zIndex(zIndex).drawOpacity(p)
}
val fadeOutTransition: CrossTransitionModifier = { progress, state ->
    val target = state == ItemTransitionState.BecomingVisible
    val p = if (target) 1f else min(1f - progress, .99f)
    val zIndex = if (target) 0f else 1f
    Modifier.zIndex(zIndex).drawOpacity(p)
}

/**
 * This function returns the Modifier which is applied to both screens the current and the new one.
 * progress -> is the progress of the transition 0f..1f
 * state -> is true when the modifier is applied to the new screen
 *                 and false for the screen which is replaced by the new screen
 */
typealias CrossTransitionModifier = (progress: Float, state: ItemTransitionState) -> Modifier

private class CrosstransitionState<T> {
    var itemA: CrosstransitionAnimationItem<T>? = null
    var itemB: CrosstransitionAnimationItem<T>? = null
    var invalidate: () -> Unit = { }
    var effect: CrossTransitionModifier? = null
    private var isTargetA: Boolean = false
    val current get() = if (isTargetA) itemA else itemB
    fun newTarget(item: CrosstransitionAnimationItem<T>) {
        val old = current
        isTargetA = isTargetA.not()
        itemA = if (isTargetA) item else old
        itemB = if (isTargetA) old else item
    }
    fun releaseSource() {
        if (isTargetA)
            itemB = null
        else
            itemA = null
    }
}
private data class CrosstransitionAnimationItem<T>(val key: T,val transition: CrossTransition<T>)

private typealias CrossTransition<T> = @Composable() (data: T, children: @Composable() () -> Unit) -> Unit


@Composable
fun <T: Any>NavigationSwitcher(transition: Transition<T>,
                       children: @Composable() (T) -> Unit) {
    val item = transition.data

    key(item.data) {
        val restoredRegistry by remember(item.savedState) {
            log("${item.data} Create new registry with: ${item.savedState}")
            mutableStateOf(UiSavedStateRegistry(restoredValues = item.savedState, canBeSaved = { true }))
        }
        Providers(UiSavedStateRegistryAmbient provides restoredRegistry) {
            children(item.data)
            onDispose {
                val saved = restoredRegistry.performSave()
                log("${item.data} onDispose saved: $saved")
                item.savedState = saved
            }
        }
    }
}

@Composable
inline fun <reified T: Any> NavigationTransitionSwitcher(
    transition: Transition<T>,
    noinline children: @Composable() (T) -> Unit) = NavigationTransitionSwitcher(T::class, transition, children)

@Composable
fun <T: Any> NavigationTransitionSwitcher(
    clazz: KClass<T>,
    transition: Transition<T>,
    children: @Composable() (T) -> Unit) {
    key(clazz) {
        val ts = remember { CrosstransitionState<SavedStateData<T>>() }
        val progress = animatedFloat(initVal = 0f)
        var isRunning by remember { mutableStateOf(false) }
        onCommit(transition) {
            ts.newTarget(CrosstransitionAnimationItem(transition.data) { data, children ->
                val effect = ts.effect
                val state =
                    if (data == ts.current?.key) ItemTransitionState.BecomingVisible else ItemTransitionState.BecomingNotVisible
                val modifier =
                    if (isRunning && effect != null) effect(progress.value, state)
                    else Modifier
                Box(modifier) {
                    children()
                }
            })
            if (ts.itemA?.key != ts.itemB?.key && transition.effect != null) {
                progress.snapTo(0f)
                isRunning = true
                progress.animateTo(targetValue = 1f, anim = tweenAnimation, onEnd = { _, _ ->
                    ts.invalidate();
                    ts.releaseSource()
                    isRunning = false

                })
                ts.effect = transition.effect
            }
            if (ts.effect == null) {
                ts.releaseSource()
            }
            ts.invalidate()
        }

        @Composable
        fun showItem(item: CrosstransitionAnimationItem<SavedStateData<T>>?) {
            item?.let { (item, transition) ->
                transition(item) {
                    key(item.data) {
                        log("${item.data} saved state: $${item.savedState}")
                        val restoredRegistry by remember(item.savedState) {
                            log("${item.data} Create new registry with: ${item.savedState}")
                            mutableStateOf(UiSavedStateRegistry(restoredValues = item.savedState, canBeSaved = { true }))
                        }
                        Providers(UiSavedStateRegistryAmbient provides restoredRegistry) {
                            log("${item.data} ${UiSavedStateRegistryAmbient.current}")
                            children(item.data)
                            onDispose {
                                val saved = restoredRegistry.performSave()
                                log("${item.data} onDispose saved: $saved")
                                item.savedState = saved
                            }
                        }
                    }
                }
            }
        }
        Box {
            ts.invalidate = invalidate
            showItem(item = ts.itemA)
            showItem(item = ts.itemB)
        }
    }
}
