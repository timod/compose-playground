package de.appsonair.compose

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.SystemClock
import android.provider.MediaStore
import android.util.LruCache
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asAndroidBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.platform.AmbientContext
import de.appsonair.log
import de.drick.compose.list.stateFor
import kotlinx.coroutines.*
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException
import kotlin.random.Random

val rnd = Random(SystemClock.elapsedRealtime())

suspend fun simulateDelay(delay: Long = 1000) {
    delay((rnd.nextFloat() * delay.toFloat()).toLong())
}
@Throws(IOException::class)
fun simulateError() {
    val randomFail = rnd.nextBoolean()
    if (randomFail) {
        log("Random fail simulated")
        throw IOException("Simulated loading error")
    }
}

sealed class LoadingState<out T: Any> {
    object Start: LoadingState<Nothing>()
    object Loading: LoadingState<Nothing>()
    class Error(val error: Exception): LoadingState<Nothing>()
    class Success<T: Any>(val data: T): LoadingState<T>()
}

@Composable
fun <T: Any> loadingStateFor(vararg inputs: Any?, initBlock: () -> LoadingState<T> = { LoadingState.Start },
                             loadingBlock: suspend CoroutineScope.() -> T): LoadingState<T> {
    var state by remember(*inputs) { mutableStateOf(initBlock()) }
    if (state !is LoadingState.Success) {
        LaunchedTask(*inputs) {
            val loadingSpinnerDelay = async {
                delay(500)
                state = LoadingState.Loading
            }
            state = try {
                LoadingState.Success(loadingBlock())
            } catch (err: Exception) {
                LoadingState.Error(err)
            } finally {
                loadingSpinnerDelay.cancelAndJoin()
            }
        }
    }
    return state
}

class ComposeCache<KEY, T: Any>(
    val cacheSize: Int,
    val sizeOf: (T) -> Int = {1}) {
    val cache = object: LruCache<KEY, LoadingState.Success<T>>(cacheSize) {
        override fun sizeOf(key: KEY, value: LoadingState.Success<T>): Int = sizeOf(value.data)
    }
}

object ImageCache: ResourceCache<ImageBitmap>(
    size = 20 * 1024 * 1024,
    dispose = {
        log("recycle image")
        it.asAndroidBitmap().recycle()
    },
    sizeOf = {
        it.asAndroidBitmap().byteCount
    }
)

private data class RefCounter<T>(val value: T, var counter: Int)

open class ResourceCache<T: Any>(val size: Int, val dispose: ((T) -> Unit)? = null,
                                 val sizeOf: (T) -> Int) {
    private val cache = object: LruCache<String, T>(size) {
        override fun entryRemoved(evicted: Boolean, key: String?, oldValue: T?, newValue: T?) {
            //Check if we could dispose the resource
            oldValue?.let { value ->
                // recycle image when not actively used
                if (!activeResources.contains(key)) {
                    dispose?.invoke(value)
                }
            }
        }
        override fun sizeOf(key: String?, value: T?): Int {
            return value?.let { sizeOf(value) } ?: 0
        }
    }
    private val activeResources = mutableMapOf<String, RefCounter<T>>()

    fun get(key: String) = cache.get(key)
    fun put(key: String, value: T) = cache.put(key, value)

    fun activate(id: String, image: T) {
        activeResources.getOrPut(id) {
            RefCounter(image, 0)
        }.let { it.counter++ }
        //log { "Activate: $id" }
        //logContent()
    }

    fun deActivate(id: String) {
        // recycle resource when not in cache
        activeResources[id]?.let { resCounter ->
            resCounter.counter--
            if (resCounter.counter < 1) {
                if (cache[id] == null) {
                    log("recycle image: $id")
                    dispose?.invoke(resCounter.value)
                }
                activeResources.remove(id)
            }
        }
        //log { "Deactivate: $id" }
    }

    private fun logContent() {
        log("Active:")
        activeResources.forEach {
            log("   ${it.key} refs: ${it.value.counter}")
        }
    }
}

object ImageLoader {
    private val okHttpClientFullImage = OkHttpClient.Builder().build()

    @Composable
    fun loadImageUI(uri: Uri, retryCounter: Int = 0): LoadingState<ImageBitmap>  {
        val url = uri.toString()
        val ctx = AmbientContext.current
        val init by stateFor(uri) {
            {
                val cachedImage = ImageCache.get(url)
                if (cachedImage != null) {
                    ImageCache.activate(url, cachedImage)
                    LoadingState.Success(cachedImage)
                } else LoadingState.Start
            }
        }
        onDispose {
            ImageCache.deActivate(url)
        }
        return loadingStateFor(url, retryCounter, initBlock = init) {
            // Start loading the image and await the result.
            //log("Load image: $url")
            val bitmap = withContext(Dispatchers.IO) {
                //simulateDelay(5000)
                //simulateError()
                log("Loading image: $url")
                loadImage(ctx, uri)
            }
            //log("Load image success: $url")
            val img = bitmap.asImageBitmap()
            ImageCache.activate(url, img)
            ImageCache.put(url, img)
            img
        }
    }

    /**
     * Loads the image as @Composable. You can use it directly in compose functions to show UI depending on LoadingState,
     * @param retryCounter can be increased to force a reload of the image
     */
    @Composable
    fun loadImageUI(url: String, retryCounter: Int = 0): LoadingState<ImageBitmap>  {
        val init by stateFor(url) {
            {
                val cachedImage = ImageCache.get(url)
                if (cachedImage != null) {
                    ImageCache.activate(url, cachedImage)
                    LoadingState.Success(cachedImage)
                } else LoadingState.Start
            }
        }
        onDispose {
            ImageCache.deActivate(url)
        }
        return loadingStateFor(url, retryCounter, initBlock = init) {
            // Start loading the image and await the result.
            //log("Load image: $url")
            val bitmap = withContext(Dispatchers.IO) {
                //simulateDelay(5000)
                //simulateError()
                log("Loading image: $url")
                loadImage(url)
            }
            //log("Load image success: $url")
            val img = bitmap.asImageBitmap()
            ImageCache.activate(url, img)
            ImageCache.put(url, img)
            img
        }
    }

    @Throws(IOException::class)
    fun loadImage(url: String): Bitmap {
        val request = Request.Builder().url(url).build()
        okHttpClientFullImage.newCall(request).execute().use { response ->
            if (!response.isSuccessful) {
                throw IOException("http request error code:" + response.code)
            }
            val body = response.body ?: throw IOException("http response body empty!")
            val image = body.use {
                it.byteStream().use { stream ->
                    BitmapFactory.decodeStream(stream)
                }
            }
            return image
        }
    }

    fun loadImage(ctx: Context, uri: Uri): Bitmap {
        return MediaStore.Images.Thumbnails.getThumbnail(
            ctx.contentResolver,
            uri.path!!.split("/").last().toLong(),
            MediaStore.Images.Thumbnails.MINI_KIND,
            BitmapFactory.Options()
        )
        //ctx.contentResolver.openInputStream(uri).use {
        //    return BitmapFactory.decodeStream(it)
        //}
    }
}
