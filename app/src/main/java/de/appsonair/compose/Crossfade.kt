package de.appsonair.compose

import androidx.compose.animation.animatedFloat
import androidx.compose.animation.core.AnimatedFloat
import androidx.compose.animation.core.AnimationEndReason
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha

/**
 * Same like Crossfade but it is initialized with the current child. So it will not fade in from null.
 */
@Composable
fun <T> InitializedCrossfade(current: T, children: @Composable() (T) -> Unit) {
    val state = remember { CrossfadeState(current) }
    if (current != state.current) {
        state.current = current
        val keys = state.items.map { it.key }.toMutableList()
        if (!keys.contains(current)) {
            keys.add(current)
        }
        state.items.clear()
        keys.mapTo(state.items) { key ->
            CrossfadeAnimationItem(key) { children ->
                val opacity = animatedOpacity(
                        visible = key == current,
                        onAnimationFinish = {
                            if (key == state.current) {
                                // leave only the current in the list
                                state.items.removeAll { it.key != state.current }
                                state.invalidate()
                            }
                        }
                )
                Box(Modifier.alpha(opacity.value)) {
                    children()
                }
            }
        }
    }
    Box {
        state.invalidate = invalidate
        state.items.forEach { (item, opacity) ->
            key(item) {
                opacity {
                    children(item)
                }
            }
        }
    }
}

private class CrossfadeState<T>(initState: T) {
    var current: T = initState
    var items = mutableListOf(CrossfadeAnimationItem(initState, { children -> children() }))
    var invalidate: () -> Unit = { }
}

private data class CrossfadeAnimationItem<T>(
        val key: T,
        val transition: CrossfadeTransition
)

private typealias CrossfadeTransition = @Composable() (children: @Composable() () -> Unit) -> Unit

@Composable
fun animatedOpacity(
        visible: Boolean,
        onAnimationFinish: () -> Unit = {}
): AnimatedFloat {
    val animatedFloat = animatedFloat(if (!visible) 1f else 0f)
    onCommit(visible) {
        animatedFloat.animateTo(
                if (visible) 1f else 0f,
                anim = tween(500),
                onEnd = { reason, _ ->
                    if (reason == AnimationEndReason.TargetReached) {
                        onAnimationFinish()
                    }
                })
    }
    return animatedFloat
}
