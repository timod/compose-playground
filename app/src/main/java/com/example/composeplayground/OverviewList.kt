package com.example.composeplayground

import android.content.res.Configuration
import android.net.Uri
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.gesture.scrollorientationlocking.Orientation
import androidx.compose.ui.platform.AmbientConfiguration
import androidx.compose.ui.unit.dp
import de.appsonair.observable
import de.drick.compose.list.*
import kotlinx.coroutines.CoroutineScope

enum class ItemAction {
    NO_ACTION, NEW, DELETING
}

data class ImageItem(val index: Int, val imageDetails: com.example.network.ImageDetails, var state: ItemAction = ItemAction.NO_ACTION)

class OverviewList(coroutineScope: CoroutineScope) : CoroutineScope by coroutineScope {
    private val api = com.example.network.LoremPicsumApi

    private var imageListObservable = observable(listOf<ImageItem>())
    private var imageList by imageListObservable
    private var counter = 0
    private val listFlow = pagingFlow { page ->
        api.getImageList(10, page).map {
            ImageItem(counter++, it)
        }
    }.stateList()

    private fun deleteItem(item: ImageItem) {
        val list = imageList.toMutableList()
        list.remove(item)
        imageList = list
        imageListObservable.notifyObservers()
    }

    @Composable
    fun ui(onImageClicked: (com.example.network.ImageDetails) -> Unit) {
        var retryCounter = remember { 0 }
        val showVerical = AmbientConfiguration.current.orientation == Configuration.ORIENTATION_PORTRAIT
        ContentView(
            modifier = Modifier.fillMaxSize(),
            showVerticalList = showVerical,
            listFlow = listFlow,
            onReloadList = { retryCounter++ },
            onImageClicked = onImageClicked,
            onDeleteItem = { deleteItem(it) }
        )
    }
}

@Composable
fun ImageCardView(uri: Uri, author: String) {
    key(uri, author) {
        Box(modifier = Modifier.aspectRatio(1.778f)) {
            ImageView(uri)
            ImageCaption(title = "Author: $author")
        }
    }
}

@Composable
fun ImageCardView(url: String, author: String) {
    Box(modifier = Modifier.aspectRatio(1.778f)) {
        ImageView(url = url)
        ImageCaption(title = "Author: $author")
    }
}

@Composable
fun ContentView(
    modifier: Modifier,
    listFlow: Flow2StateList<ImageItem>,
    showVerticalList: Boolean = true,
    onReloadList: () -> Unit,
    onDeleteItem: (ImageItem) -> Unit,
    onImageClicked: (com.example.network.ImageDetails) -> Unit
) {

    Box(modifier, contentAlignment = Alignment.Center) {
        val padding = 12.dp
        LayoutFlowList  (
            flow = listFlow,
            orientation = Orientation.Vertical
            /*MultiColumnList(
            lines = 2,
            listPosition = listPosition,
            orientation = if (showVerticalList) Orientation.Vertical else Orientation.Horizontal,
            items = list
            //onDelete = onDeleteItem,
            //deleteText = { "Remove ${it.imageDetails.author}" }*/
        ) { itemState ->
            //LazyColumnItems(items = list){ item ->
            //Row {
            //    for (item in itemList) {
                    key(itemState) {
                        Surface(
                            elevation = 8.dp,
                            shape = RoundedCornerShape(6.dp),
                            modifier = Modifier
                                //.weight(1f)
                                .padding(padding)
                                .clickable(onClick = {
                                    if (itemState is FlowState.Item) onImageClicked(itemState.value.imageDetails)
                                })
                        ) {
                            when (itemState) {
                                is FlowState.Loading -> {
                                    Box(Modifier.fillMaxWidth()) {
                                        CircularProgressIndicator()
                                    }
                                }
                                is FlowState.Item -> {
                                    val item = itemState.value
                                    val url = "https://picsum.photos/id/${item.imageDetails.id}/600/400"
                                    ImageCardView(url, item.index.toString())
                                }
                                is FlowState.Error -> {
                                    Box(Modifier.fillMaxSize(), contentAlignment = Alignment.BottomCenter) {
                                        Column(modifier = Modifier.padding(6.dp)) {
                                            Text("Unable to load list! ${itemState.error}")
                                            OutlinedButton(onClick = { listFlow.retry() }) {
                                                Text("Retry")
                                            }
                                        }
                                    }
                                }
                            }
                        }
            //        }
            //    }
            }
        }

        /*when (val state: LaodingState<List<ImageDetails>> = LoadingState.Success(imageList)) {
            is LoadingState.Start -> {
                Text(text = "Start")
            }
            is LoadingState.Loading -> {
                CircularProgressIndicator()
            }
            is LoadingState.Success -> {
                VerticalList(data = state.data, scrollerPosition = position) {
                    Clickable(onClick = { onImageClicked(it) }) {
                        ImageCardView(it)
                    }

                }
            }
            is LoadingState.Error -> {
                Box(Modifier.fillMaxSize(), gravity = Alignment.BottomCenter) {
                    Column(modifier = Modifier.padding(6.dp)) {
                        Text("Unable to load list! ${state.error}")
                        OutlinedButton(onClick = { onReloadList() }) {
                            Text("Retry")
                        }
                    }
                }
            }
        }*/
    }
}

