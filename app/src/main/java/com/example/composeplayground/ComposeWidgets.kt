package com.example.composeplayground

import androidx.compose.animation.animatedFloat
import androidx.compose.foundation.animation.FlingConfig
import androidx.compose.foundation.animation.fling
import androidx.compose.foundation.gestures.draggable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.*
import androidx.compose.ui.*
import androidx.compose.ui.gesture.scrollorientationlocking.Orientation
import androidx.compose.ui.layout.*
import androidx.compose.ui.unit.Constraints
import de.appsonair.log
import kotlin.math.roundToInt

enum class Visibility {
    VISIBLE, INVISIBLE, GONE
}

fun Modifier.visibility(visibility: Visibility) = this.then(VisibilityModifier(visibility))

class VisibilityModifier(private val visibility: Visibility) : LayoutModifier {
    override fun MeasureScope.measure(
        measurable: Measurable,
        constraints: Constraints
    ): MeasureResult = if (visibility == Visibility.GONE) {
        layout(0, 0) {
            // Empty placement block
        }
    } else {
        val placeable = measurable.measure(constraints)
        layout(placeable.width, placeable.height) {
            if (visibility == Visibility.VISIBLE) {
                placeable.place(0, 0)
            }
        }
    }
}

enum class SwipeAreaDirection {
    START2END, // Swipe from left to right when LTR LayoutDirection is active
    END2START, // Swipe from right to left when LTR LayoutDirection is active
    BOTH
}

enum class SwipeState {
    CENTER, START, END, REMOVED
}

@Composable
fun SwipeAreaLayout(
    modifier: Modifier = Modifier,
    orientation: Orientation = Orientation.Vertical,
    direction: SwipeAreaDirection = SwipeAreaDirection.BOTH,
    onSwiped: (SwipeState) -> Unit,
    enabled: Boolean = true,
    background: @Composable() () -> Unit = { Box(if (orientation == Orientation.Vertical) Modifier.fillMaxWidth() else Modifier.fillMaxHeight()) },
    child: @Composable() () -> Unit = emptyContent()
) {
    var parentSize = remember { 0 }
    val position = animatedFloat(0f)
    var flingConfig: FlingConfig? = remember { null }
    var state: SwipeState = remember { SwipeState.CENTER }
    var dragInProgress = remember { false }
    log("Render")
    if (state == SwipeState.REMOVED && !position.isRunning) {
        log("Removing...")
        val w = parentSize.toFloat()
        position.setBounds(-w, w)
        position.animateTo(
            if (position.value > 0) w else -w,
            onEnd = { _, _ -> onSwiped(SwipeState.REMOVED); state = SwipeState.REMOVED })
    }
    Layout(
        modifier = Modifier.draggable(
            startDragImmediately = position.isRunning,
            orientation = orientation,
            onDragStarted = { dragInProgress = true },
            onDragStopped = { velocity ->
                dragInProgress = false
                flingConfig?.let {
                    position.fling(startVelocity = velocity, config = it) { _, endValue, _ ->
                        log("Fling end $endValue")
                        val newState = when {
                            endValue > 0f -> SwipeState.END //TODO check for RTL
                            endValue < 0f -> SwipeState.START
                            else -> SwipeState.CENTER
                        }
                        onSwiped(newState)
                        state = newState
                    }
                }
            },
            onDrag = { delta ->
                position.snapTo(position.value + delta)
            },
            enabled = enabled
        ),
        content = {
            background()
            child()
        },
        measureBlock = { measurables, constraints ->
            require(measurables.size == 2) { "background and child must only contain 1 root element" }
            parentSize = if (orientation == Orientation.Vertical) constraints.maxHeight else constraints.maxWidth
            val bgPlaceable = measurables.first().measure(constraints)
            val childPlaceable = measurables.last().measure(constraints)
            val size = if (orientation == Orientation.Horizontal) bgPlaceable.height.toFloat() else bgPlaceable.width.toFloat()
            if (dragInProgress) {
                //log("Create fling config size: $size bg placeable: ${bgPlaceable.height} x ${bgPlaceable.width}")
                flingConfig = FlingConfig(anchors = listOf(-size, 0f, size))
            }
            when (direction) {
                SwipeAreaDirection.START2END -> position.setBounds(-size, 0f)
                SwipeAreaDirection.END2START -> position.setBounds(0f, size)
                SwipeAreaDirection.BOTH -> position.setBounds(-size, size)
            }
            layout(childPlaceable.width, childPlaceable.height) {
                bgPlaceable.place(0, 0)
                if (orientation == Orientation.Vertical)
                    childPlaceable.place(position.value.roundToInt(), 0)
                else
                    childPlaceable.place(0, position.value.roundToInt())
            }
        }
    )
}
