package com.example.composeplayground

import android.os.Bundle
import android.view.WindowManager
import androidx.activity.ComponentActivity
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.setContent
import androidx.compose.ui.unit.dp
import de.appsonair.compose.*
import de.appsonair.log
import de.drick.compose.list.VerticalLayoutList
import de.drick.compose.list.layoutListPosition
import de.drick.compose.list.state

@Composable
fun item(i: Int, modifier: Modifier) {
    key(i) {
        val list by state {
            (i..i + 5).mapIndexed { index, item ->
                ComposeRemoveAreaActivity.Item(
                    index,
                    "$item"
                )
            }
        }
        val listPosition = layoutListPosition()
        onActive {
            log("Active")
            onDispose {
                log("Dispose")
            }
        }
        Surface(
            //modifier = Modifier.fillFractionWidth(if (even) 1f else 0.5f).padding(8.dp),
            modifier = modifier.padding(8.dp),
            elevation = 8.dp,
            shape = RoundedCornerShape(6.dp),
            color = Color.LightGray
        ) {
            key(list) {
                log("Render list")
                VerticalLayoutList(items = list, listPosition = listPosition) { item ->
                    val url = "https://picsum.photos/id/${item.index}/600/400"
                    ImageCardView(
                        url = url,
                        author = "${item.text} - Das ist ein Test"
                    )
                }
            }
        }
    }
}

@Composable
fun test(index: Int, text: String) {
    var i by state { Transition(data = SavedStateData(index, emptyMap()), effect = slideInTransition) }
    /*ItemSwitcher(current = i, transitionDefinition = transition1) { index, state ->
        item(index.data, slideInTransitionModifier(state).clickable(onClick = { i = Transition(data = index.data+1, effect = slideInTransition) }))
    }*/
    NavigationTransitionSwitcher(transition = i) { index ->
        item(index, Modifier.clickable(onClick = { i = Transition(data = SavedStateData(index + 1, emptyMap()), effect = slideInTransition) }))
    }
}

class ComposeRemoveAreaActivity : ComponentActivity() {

    data class Item(val index: Int, val text: String)

    private val modelList = mutableStateListOf<Item>()

    private fun removeItem(item: Item) {
        //modelList.remove(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER, WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER)

        setContent {
            val range = 0..100
            val list: Array<Item> = range.mapIndexed { index, item -> Item(index, "$item") }.toTypedArray()
            modelList.addAll(list)
            MaterialTheme {
                VerticalLayoutList(items = modelList) { item ->
                    val even = item.index % 2 == 0
                    var visible by state { Visibility.VISIBLE }
                    Box(
                        modifier = Modifier
                            .visibility(visibility = visible)
                            .clickable(onClick = { visible = if (visible == Visibility.VISIBLE) Visibility.INVISIBLE else Visibility.GONE })
                        ,
                        contentAlignment = Alignment.Center
                    ) {
                        Surface(
                            //modifier = Modifier.fillFractionWidth(if (even) 1f else 0.5f).padding(8.dp),
                            modifier = Modifier
                                //.fillFractionHeight(if (even) 1f else 0.5f)
                                .padding(8.dp),
                            elevation = 8.dp,
                            shape = RoundedCornerShape(6.dp),
                            color = Color.LightGray
                        ) {
                            ImageCardView(
                                item.index.toString(),
                                "${item.text} - Das ist ein Test"
                            )
                        }
                    }
                }

                /*DynamicList(modifier = Modifier.fillMaxSize(), data = modelList,
                    onDelete = { modelList.remove(it) }, deleteText = { "Remove ${it.second}" }) {
                    val (index, item) = it
                    val even = index % 2 == 0
                    /*onActive {
                        log("Active: $item")
                        onDispose {

                            log("Deactivate: $item")
                        }
                    }*/
                    Row(Modifier.padding(6.dp)) {
                        Box(Modifier.fillMaxWidth(), backgroundColor = Color.LightGray) {
                            Text(
                                modifier = Modifier.padding(if (even) 6.dp else 30.dp),
                                text = "$item - Das ist ein Test"
                            )
                        }
                    }
                }*/

            }
        }
    }
}

/*@Composable
fun <T: Any>DynamicList(modifier: Modifier = Modifier, listPosition: ListPosition<T> = ListPosition(), data: List<T>,
                        onDelete: (T) -> Unit, deleteText: (T) -> String,
                        itemCallback: @Composable() (T) -> Unit) {
    val collapsedList = remember(data) { mutableStateListOf<T>() }
    Stack() {
        VerticalList(data = data, modifier = modifier, listPosition = listPosition) { item ->
            val collapsed = item in collapsedList
            /*CollapseableArea(
                item,
                collapsed = if (collapsed) CollapseState.COLLAPSED else CollapseState.EXPANDED
            ) {
                //SwipeArea(onSwiped = { if (it) collapsedList.add(item) }) {
                    itemCallback(item)
                //}
            }*/
        }
        Column(modifier = Modifier.gravity(Alignment.BottomCenter)) {
            collapsedList.forEach {
                SnackbarUI(modifier = Modifier.padding(6.dp), text = deleteText(it), actionText = "Undo",
                    onAction = {
                        collapsedList.remove(it)
                        log("action $it")
                    },
                    onDismiss = {
                        collapsedList.remove(it)
                        onDelete(it)
                        log("Dismiss $it")
                    }
                )
            }
        }
    }
}
*/
