package com.example.composeplayground

import android.net.Uri
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import de.appsonair.compose.ImageLoader
import de.appsonair.compose.InitializedCrossfade
import de.appsonair.compose.LoadingState
import de.drick.compose.list.stateFor


@Composable
fun LoadingBox() {
    Box(Modifier.fillMaxSize().background(MaterialTheme.colors.surface), contentAlignment = Alignment.Center) {
        CircularProgressIndicator()
    }
}

@Composable
fun ErrorBox(errorState: LoadingState.Error, onClick: () -> Unit) {
    Box(Modifier.fillMaxSize().background(MaterialTheme.colors.surface), contentAlignment = Alignment.Center) {
        Column {
            OutlinedButton(
                modifier = Modifier.align(Alignment.CenterHorizontally),
                onClick = onClick) {
                Text("Retry")
            }
            Text("Error: ${errorState.error.message}")
        }
    }
}

@Composable
fun ImageView(uri: Uri) {
    var retryCounter by remember(uri) { mutableStateOf(0) }
    val imageState = ImageLoader.loadImageUI(uri)
    //val state = imageState
    InitializedCrossfade(current = imageState) { state ->
        when (state) {
            is LoadingState.Start -> Box(Modifier.fillMaxSize().background(MaterialTheme.colors.surface))
            is LoadingState.Loading -> LoadingBox()
            is LoadingState.Success -> Image(bitmap = state.data, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.Crop)
            is LoadingState.Error -> ErrorBox(state, onClick = { retryCounter++ })
        }
    }
}

@Composable
fun ImageView(url: String) {
    var retryCounter by stateFor(url) { 0 }
    val imageState = ImageLoader.loadImageUI(url)
    //val state = imageState
    InitializedCrossfade(current = imageState) { state ->
        when (state) {
            is LoadingState.Start -> Box(Modifier.fillMaxSize().background(MaterialTheme.colors.surface))
            is LoadingState.Loading -> LoadingBox()
            is LoadingState.Success -> Image(bitmap = state.data, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.Crop)
            is LoadingState.Error -> ErrorBox(state, onClick = { retryCounter++ })
        }
    }
}

@Composable
fun ImageCaption(title: String) {
    key(title) {
        val typography = MaterialTheme.typography
        val headerStyle = typography.subtitle1.copy(
            color = Color.White,
            shadow = Shadow(blurRadius = 8f)
        )
        Surface(
            elevation = 0.dp,
            color = Color(0f, 0f, 0f, .5f),
            modifier = Modifier.fillMaxWidth()
        ) {
            Row {
                Text(modifier = Modifier.padding(6.dp), text = title, style = headerStyle)
            }
        }
    }
}

/*
@Composable
fun MainView(@Pivotal imageListState: LoadingState<List<ImageDetails>>, onReloadList: () -> Unit) {
    val scaffoldState = remember { ScaffoldState() }
    // Consider negative values to mean 'cut corner' and positive values to mean 'round corner'
    val sharpEdgePercent = -50f
    val roundEdgePercent = 45f
    // Start with sharp edges
    val animatedProgress = animatedFloat(sharpEdgePercent)
    // animation value to animate shape
    val progress = animatedProgress.value.roundToInt()

    // When progress is 0, there is no modification to the edges so we are just drawing a rectangle.
    // This allows for a smooth transition between cut corners and round corners.
    val fabShape = if (progress < 0) {
        CutCornerShape(abs(progress))
    } else if (progress == roundEdgePercent.toInt()) {
        CircleShape
    } else {
        RoundedCornerShape(progress)
    }
    // lambda to call to trigger shape animation
    val changeShape = {
        val target = animatedProgress.targetValue
        val nextTarget = if (target == roundEdgePercent) sharpEdgePercent else roundEdgePercent
        animatedProgress.animateTo(
            targetValue = nextTarget,
            anim = TweenBuilder<Float>().apply { duration = 600 }
        )
    }


    AppTheme {
        Greeting(name = "Wurst")
        Scaffold(
            scaffoldState = scaffoldState,
            drawerContent = { Text("Drawer content") },
            topAppBar = { TopAppBar(title = { Text("Scaffold with bottom cutout") }) },
            bottomAppBar = { fabConfiguraion ->
                BottomAppBar(
                    fabConfiguration = fabConfiguraion,
                    cutoutShape = fabShape
                ) {
                    AppBarIconVector(R.drawable.ic_launcher_foreground) {
                        scaffoldState.drawerState = DrawerState.Opened
                    }
                }
            },
            floatingActionButton = {
                FloatingActionButton(
                    onClick = changeShape,
                    shape = fabShape,
                    color = MaterialTheme.colors().secondary
                ) {
                    Text("Change shape", modifier = LayoutPadding(12.dp))
                }
            },
            floatingActionButtonPosition = Scaffold.FabPosition.CenterDocked,
            bodyContent = { modifier ->
                //ContentView(modifier, imageListState, onReloadList)
            }
        )
    }
}
*/