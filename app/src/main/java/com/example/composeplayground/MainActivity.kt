package com.example.composeplayground

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.*
import androidx.compose.runtime.savedinstancestate.savedInstanceState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.platform.setContent
import androidx.compose.ui.unit.dp
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingSource
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.items
import com.example.network.ImageDetails
import com.example.network.LoremPicsumApi
import de.appsonair.compose.*
import de.appsonair.log

val lightThemeColors = lightColors(
        primary = Color(0xFF448AFF),
        onPrimary = Color.White,
        secondary = Color(0xFFFAB24A),
        onSecondary = Color.Black,
        background = Color(0xFFFAB24A),
        onBackground = Color.Black,
        surface = Color(0xFFFFD69C),
        onSurface = Color.Black,
        error = Color(0xFFD00036),
        onError = Color.White
)

@Composable
fun AppTheme(children: @Composable() () -> Unit) {
    MaterialTheme(colors = lightThemeColors) {
        children()
    }
}

class MainActivity : ComponentActivity() {

    /**
     * Initialisation of the navigation stack. First screen is StartScreen.
     * To survive activity recreations we could specify this variable outside of the Activity.
     */
    private val navigation = NavigationStack<MainUIScreen>(MainUIScreen.StartScreen)

    /**
     * Definition of screens for the Navigation.
     */
    private sealed class MainUIScreen {
        object StartScreen: MainUIScreen()
        object Overview: MainUIScreen()
        data class Detail(val imageDetails: ImageDetails): MainUIScreen()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AppTheme {
                NavigationTransitionSwitcher(transition = navigation.getTransition()) { screen -> // screen which should be shown
                    when (screen) {
                        // Show the StartScreen
                        is MainUIScreen.StartScreen -> StartScreen(onStartClicked = {
                            // Navigate to the Overview screen using crossfade transition
                            navigation.next(
                                MainUIScreen.Overview,
                                enterTransition = crossfadeTransition,
                                exitTransition = crossfadeTransition
                            )
                        })
                        // Show the Overview screen
                        is MainUIScreen.Overview -> Overview() { imageDetails ->
                            // Navigate to Detail screen using roll left/right transition
                            navigation.next(
                                MainUIScreen.Detail(imageDetails),
                                enterTransition = slideRollLeftTransition,
                                exitTransition = slideRollRightTransition
                            )
                        }
                        is MainUIScreen.Detail -> DetailView(imageDetails = screen.imageDetails)
                    }
                }
            }
        }
    }
}

@Composable
fun StartScreen(onStartClicked: () -> Unit) {
    var checkBoxState by savedInstanceState { false } // state will be restored when user navigates to the next screen and later come back to this screen.
    log("Checkbox state: $checkBoxState")
    Box(modifier = Modifier.fillMaxSize()) {
        Column(modifier = Modifier
            .align(Alignment.Center)
            .padding(12.dp)) {
            Checkbox(checked = checkBoxState, onCheckedChange = { checkBoxState = it })
            Button(
                modifier = Modifier.padding(8.dp),
                onClick = onStartClicked
            ) {
                Text("Start")
            }
        }
    }
}

@Composable
fun Overview(onImageClicked: (ImageDetails) -> Unit) {
    var retryCounter by remember { mutableStateOf(0) } // Just to retry the loading. So increasing this value will trigger a reload
    val pager = remember {
        Pager(PagingConfig(pageSize = 10)) {
            object : PagingSource<Int, ImageDetails>() {
                override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ImageDetails> {
                    return try {
                        val pageNumber = params.key ?: 1
                        log("Load page: $pageNumber")
                        val response = LoremPicsumApi.getImageList(10, pageNumber)
                        val prevKey = if (pageNumber > 1) pageNumber - 1 else null
                        val nextKey = if (response.isNotEmpty()) pageNumber + 1 else null
                        LoadResult.Page(data = response, prevKey = prevKey, nextKey = nextKey)
                    } catch (err: Throwable) {
                        LoadResult.Error(err)
                    }
                }
            }
        }
    }
    val lazyPagingItems: LazyPagingItems<ImageDetails> = pager.flow.collectAsLazyPagingItems()
    LazyColumn(modifier = Modifier.fillMaxSize()) {
        items(lazyPagingItems) { item ->
            if (item != null) {
                Surface(
                    elevation = 8.dp,
                    shape = RoundedCornerShape(6.dp),
                    modifier = Modifier
                        .padding(12.dp)
                        .clickable(onClick = { item.let(onImageClicked) })
                ) {
                    val url = "https://picsum.photos/id/${item.id}/600/400"
                    ImageCardView(url, item.author ?: "NA")
                }
            }
        }
    }
    // Load list of images in background. LoadingState contains the loading state (Working, Result, Error)
    /*val loadingState = progressStateFor(retryCounter) {
        log("Load image list")
        com.example.network.LoremPicsumApi.getImageList(100, 0)
    }
    when (loadingState) {
        is ProgressState.Working -> {
            Box(Modifier.fillMaxSize()) {
                CircularProgressIndicator()
            }
        }
        is ProgressState.Result -> {
            val imageList = loadingState.value
            MyList(imageList, onImageClicked)
        }
        is ProgressState.Error -> {
            Box(Modifier.fillMaxSize(), gravity = Alignment.BottomCenter) {
                Column(modifier = Modifier.padding(6.dp)) {
                    Text("Unable to load list! ${loadingState.error}")
                }
                OutlinedButton(onClick = { retryCounter++ }) {
                    Text("Retry")
                }
            }
        }
    }*/
}

@Composable
fun MyList(list: List<ImageDetails>, onImageClicked: (ImageDetails) -> Unit) {
    val padding = 12.dp
    Box(Modifier.fillMaxSize()) {
        // Alternative of LazyColumnFor which preserve the scrolling position.
        //VerticalLayoutList(items = list) { item ->
        LazyColumn {
            items(items = list) { item ->
                Surface(
                    elevation = 8.dp,
                    shape = RoundedCornerShape(6.dp),
                    modifier = Modifier
                        .padding(padding)
                        .clickable(onClick = { onImageClicked(item) })
                ) {
                    val url = "https://picsum.photos/id/${item.id}/600/400"
                    ImageCardView(url, item.author)
                }
            }
        }
    }
}

@Composable
fun DetailView(imageDetails: com.example.network.ImageDetails) {
    val url = "https://picsum.photos/id/${imageDetails.id}/1200/800"
    val typography = MaterialTheme.typography
    val headerStyle = typography.h5.copy(
        color = Color.White,
        shadow = Shadow(blurRadius = 8f)
    )
    Surface(modifier = Modifier.fillMaxSize()) {
        Column {
            Surface(
                elevation = 0.dp,
                color = Color.Transparent.copy(alpha = .5f),
                modifier = Modifier.fillMaxWidth()
            ) {
                Text(
                    modifier = Modifier
                        .padding(6.dp)
                        .align(Alignment.CenterHorizontally),
                    text = imageDetails.author,
                    style = headerStyle
                )
            }
            Box(modifier = Modifier.aspectRatio(1.775f)) {
                log("Draw detail image")
                ImageView(url = url)
            }
        }
    }
}