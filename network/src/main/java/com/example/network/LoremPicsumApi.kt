package com.example.network

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import okhttp3.HttpUrl.Companion.toHttpUrl

@Serializable
data class ImageDetails(val id: String, val author: String, val width: Int, val height: Int, val download_url: String)

object LoremPicsumApi {
    private val jsonSerializer = Json {
        ignoreUnknownKeys = true
    }
    private val client = RestClient("https://picsum.photos".toHttpUrl(), adapterFactory = SerializationIOAdapterFactory(jsonSerializer))

    @Throws(Exception::class)
    suspend fun getImageList(itemCount: Int, page: Int = 0): List<ImageDetails> = client.get()
        .path("v2/list")
        .queryParam("limit", itemCount)
        .queryParam("page", page)
        .executeList()
}