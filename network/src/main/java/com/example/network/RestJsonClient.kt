package com.example.network

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Looper
import android.util.Base64
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.InternalSerializationApi
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.serializer
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import okio.BufferedSource
import okio.buffer
import okio.sink
import java.io.File
import java.io.IOException
import java.net.UnknownHostException
import java.nio.charset.StandardCharsets
import kotlin.reflect.KClass

/**
 * @author Timo Drick
 */

typealias InputAdapter<T> = (BufferedSource) -> T
typealias OutputAdapter<T> = (T) -> RequestBody

interface IOAdapterFactory {
    fun <T: Any>getInputAdapter(kClass: KClass<T>): InputAdapter<T>
    fun <T: Any>getListInputAdapter(kClass: KClass<T>): InputAdapter<List<T>>
    fun <T: Any>getOutputAdapter(kClass: KClass<T>): OutputAdapter<T>
}

@OptIn(InternalSerializationApi::class)
class SerializationIOAdapterFactory(private val json: Json = Json) : IOAdapterFactory {
    override fun <T : Any> getInputAdapter(kClass: KClass<T>): InputAdapter<T> = { source ->
        if (kClass == List::class) {
            error("Use executeList to load List<T>!")
        }
        json.decodeFromString(kClass.serializer(), source.readUtf8())
    }
    override fun <T : Any> getListInputAdapter(kClass: KClass<T>): InputAdapter<List<T>> = { source ->
        json.decodeFromString(ListSerializer(kClass.serializer()), source.readUtf8())
    }
    override fun <T : Any> getOutputAdapter(kClass: KClass<T>): OutputAdapter<T> = { data ->
        json.encodeToString(kClass.serializer(), data).toRequestBody("application/json".toMediaType())
    }
}
/*class MoshiIOAdapterFactory(private val moshi: Moshi = Moshi.Builder().build()) : IOAdapterFactory {
    override fun <T : Any> getInputAdapter(kClass: KClass<T>): InputAdapter<T> = { source ->
        moshi.adapter(kClass.java).fromJson(source) ?: throw IOException("Unable to parse moshi data!")
    }
    override fun <T : Any> getOutputAdapter(kClass: KClass<T>): OutputAdapter<T> = { data ->
        moshi.adapter(kClass.java).toJson(data).toRequestBody("application/json".toMediaType())
    }
}*/

private val bitmapInputAdapter: InputAdapter<Bitmap> = { source ->
    BitmapFactory.decodeStream(source.inputStream())
}

@Throws(IOException::class)
suspend fun RestClient.URLScope.getBitmap(): Bitmap = execute(bitmapInputAdapter)

@Throws(IOException::class)
suspend fun RestClient.URLScope.getFile(targetFile: File): File = execute { source ->
    targetFile.sink().buffer().use { it.writeAll(source) }
    targetFile
}

open class RestClient(
    val host: HttpUrl,
    okHttpClient: OkHttpClient? = null,
    val adapterFactory: IOAdapterFactory,
    val configuration: ConfigScope.() -> Unit = {}
) {

    private val client: OkHttpClient

    init {
        val clientBuilder = okHttpClient?.newBuilder() ?: OkHttpClient.Builder()
        val cs = ConfigScope(clientBuilder)
        configuration(cs)
        client = clientBuilder.build()
    }

    inner class ConfigScope(val clientBuilder: OkHttpClient.Builder) {
        private val headerList = mutableListOf<Pair<String, String>>()
        private val headerInterceptor: Interceptor = object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val rb = chain.request().newBuilder()
                headerList.forEach {
                    rb.addHeader(it.first, it.second)
                }
                return chain.proceed(rb.build())
            }
        }
        fun addHeader(name: String, value: String) {
            if (headerList.size < 1) { //add header interceptor
                addInterceptor(headerInterceptor)
            }
            headerList.add(Pair(name, value))
        }
        fun addInterceptor(interceptor: Interceptor) {
            clientBuilder.addInterceptor(interceptor)
        }
        fun addNetworkInterceptor(interceptor: Interceptor) {
            clientBuilder.addNetworkInterceptor(interceptor)
        }
        fun addBasicAuth(user: String, password: String) {
            val authHeaderKey = "Authorization"

            val userAndPassword = "$user:$password"
            val encoded = Base64.encodeToString(userAndPassword.toByteArray(StandardCharsets.US_ASCII), Base64.NO_WRAP)
            val credentials = "Basic $encoded"

            addHeader(authHeaderKey, credentials)
        }
    }

    inner class MethodScope(private val requestBuilder: Request.Builder) {
        fun path(path: String) = URLScope(requestBuilder, host.newBuilder().addPathSegments(path))
        fun url(url: HttpUrl) = URLScope(requestBuilder, url.newBuilder())
    }

    inner class URLScope internal constructor(private val requestBuilder: Request.Builder, private val urlBuilder: HttpUrl.Builder) {
        fun queryParam(name: String, value: Any): URLScope {
            urlBuilder.addQueryParameter(name, value.toString())
            return this
        }
        suspend inline fun <reified T: Any>execute() = execute(adapterFactory.getInputAdapter(T::class))
        suspend inline fun <reified T: Any>executeList() = execute(adapterFactory.getListInputAdapter(T::class))
        suspend fun <T: Any>execute(inputAdapter: InputAdapter<T>): T = withContext(Dispatchers.IO) {
            execute(inputAdapter, requestBuilder.url(urlBuilder.build()).build())
        }
        suspend fun executeNoResponse() = withContext(Dispatchers.IO) {
            execute(requestBuilder.url(urlBuilder.build()).build())
        }
    }

    fun get() = MethodScope(Request.Builder().get())
    fun post(requestBody: RequestBody) = MethodScope(Request.Builder().post(requestBody))
    fun <T: Any>post(adapter: OutputAdapter<T>, data: T) = post(adapter(data))
    inline fun <reified T: Any>post(data: T) = post(adapterFactory.getOutputAdapter(T::class), data)

    @Throws(IOException::class)
    fun <T: Any> execute(inputAdapter: InputAdapter<T>, request: Request): T = requireNotNull(executeRaw(inputAdapter, request))

    @Throws(IOException::class)
    fun execute(request: Request) {
        executeRaw<Nothing>(null, request)
    }

    @Throws(IOException::class)
    fun <T: Any> executeRaw(inputAdapter: InputAdapter<T>?, request: Request): T? {
        //log("Request: ${request.url}")
        check(Looper.myLooper() != Looper.getMainLooper()) { "Network operation on main Thread!" }
        try {
            client.newCall(request).execute().use { response ->
                checkResponse(response)
                if (inputAdapter != null) {
                    response.body?.use { body ->
                        try {
                            return inputAdapter(body.source())
                        } catch (err: Throwable) {
                            //log(err.message ?: "Error!", err)
                            throw err
                        }
                    }
                } else {
                    return null
                }
            }
        } catch (err: UnknownHostException) {
            throw NoInternetConnection()
        }
        throw IOException("Unable to get object!")
    }

    @Throws(IOException::class)
    private fun checkResponse(response: Response) {
        when(val code = response.code) {
            in 200..300 -> return
            401 -> throw AuthException(response.message)
            429 -> {
                val retryDelay = response.header("Retry-After")?.toInt() ?: 1
                throw TooManyRequests(retryDelay)
            }
            503 -> throw ServiceUnavailableException()
            else -> throw IOException("Server returned code: $code:" + response.message)
        }
    }

    /*private fun hasInternetConnectivity(): Boolean {
        val cm = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= 23) {
            // Check for internet connectivity
            cm.activeNetwork?.let { an ->
                cm.getNetworkCapabilities(an)?.let { nc ->
                    return nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) && nc.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED)
                }
            }
        }
        //TODO check for API level < 23
        return true
    }*/
}

class AuthException(message: String): IOException(message)
class ServiceUnavailableException: IOException()
class TooManyRequests(val retryDelay: Int): IOException("Too many requests. Retry in $retryDelay seconds")
class NoInternetConnection: IOException("No internet connection!")