# Compose Playground

This project contains several missing components to build Compose only apps (Single Activity)

- Navigation
- Transition between screens switches
- Alternative list implementation to LazyColumnFor and LazyRowFor
- progress state handling helpers

Documentation is work in progress....

The file MainActivity.kt demonstrates simple usage of the navigation and list framework.